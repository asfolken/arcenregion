<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @property int nb_point
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{

    const ALL_PROTOCOLE = "all";
    const NO_PROTOCOLE = "np";
    const ONLY_CHOSEN_PROTOCOLE = "ocp";

    const RULES_PROTOCOLE = [
        "Tous les protocoles" => self::ALL_PROTOCOLE,
        "Aucun protocole" => self::NO_PROTOCOLE,
        "Choisir les protocoles autorisés" => self::ONLY_CHOSEN_PROTOCOLE,
    ];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Log", mappedBy="user", cascade={"all"})
     */
    private $logs;

    /**
     * @ORM\OneToMany(targetEntity="Todo", mappedBy="auteur", cascade={"all"})
     */
    private $todoAuteurs;

    /**
     * @var string
     *
     * @ORM\Column(name="rulesProtocole", type="string", length=255, nullable=true)
     */
    private $rulesProtocole;

    /**
     * @ORM\ManyToMany(targetEntity="Todo", mappedBy="destinataires", cascade={"persist"}, fetch="EXTRA_LAZY"))
     */
    private $todoDestinataires;

    /**
     * @ORM\ManyToMany(targetEntity="Essais", inversedBy="users", cascade={"persist"}, fetch="EXTRA_LAZY"))
     * @ORM\JoinTable(name="users_essais")
     */
    private $essais;

    /**
     * @var ArrayCollection|EI[]
     * @ORM\ManyToMany(targetEntity="EI", mappedBy="users", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $eis;


    /**
     * @var ArrayCollection|Essais[]
     * @ORM\OneToMany(targetEntity="Essais", mappedBy="createdBy", cascade={"persist"})
     */
    private $mesEssais;


    /**
     * @ORM\ManyToMany(targetEntity="Essais", inversedBy="chosenBys", cascade={"persist"}, fetch="EXTRA_LAZY") )
     * @ORM\JoinTable(name="essais_chosen_users")
     */
    private $chosenEssais;


    /**
     * Add essai
     *
     * @param Essais $essai
     *
     * @return Tag
     */
    public function addChosenEssai(Essais $essai)
    {
        $this->chosenEssais[] = $essai;

        return $this;
    }

    /**
     * Remove essai
     *
     * @param Essais $essai
     */
    public function removeChosenEssai(Essais $essai)
    {
        $this->chosenEssais->removeElement($essai);
    }

    /**
     * Get essais
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChosenEssais()
    {
        return $this->chosenEssais;
    }


    /**
     * @return Essais[]|ArrayCollection
     */
    public function getMesEssais()
    {
        return $this->mesEssais;
    }

    /**
     * @param Essais $mesEssai
     * @return $this
     */
    public function addMesEssai(Essais $mesEssai)
    {
        if (!$this->mesEssais->contains($mesEssai)) {
            $this->mesEssais[] = $mesEssai;
            $mesEssai->setCreatedBy($this);
        }
        return $this;
    }

    /**
     * @param Essais $mesEssai
     * @return $this
     */
    public function removeMesEssai(Essais $mesEssai)
    {
        if ($this->mesEssais->contains($mesEssai)) {
            $this->mesEssais->removeElement($mesEssai);
            $mesEssai->setCreatedBy(null);
        }
        return $this;
    }

    public function __construct()
    {
        parent::__construct();
        $this->todoDestinataires = new ArrayCollection();
        $this->essais = new ArrayCollection();
        $this->logs = new ArrayCollection();
        $this->todoAuteurs = new ArrayCollection();
        $this->eis = new ArrayCollection();
        $this->mesEssais = new ArrayCollection();
        $this->chosenEssais = new ArrayCollection();
    }

    /**
     * @return EI[]|ArrayCollection
     */
    public function geteis()
    {
        return $this->eis;
    }

    /**
     * @param EI $ei
     * @return $this
     */
    public function addEi(EI $ei)
    {
        if (!$this->eis->contains($ei)) {
            $this->eis[] = $ei;
            $ei->addUser($this);
        }
        return $this;
    }

    /**
     * @param EI $ei
     * @return $this
     */
    public function removeEi(EI $ei)
    {
        if ($this->eis->contains($ei)) {
            $this->eis->removeElement($ei);
            $ei->removeUser($this);
        }
        return $this;
    }

    /**
     * Add log
     *
     * @param Log $log
     *
     * @return User
     */
    public function addLog(Log $log)
    {
        $this->logs[] = $log;
        $log->setUser($this);
        return $this;
    }

    /**
     * Remove log
     *
     * @param Log $log
     */
    public function removeLog(Log $log)
    {
        $this->logs->removeElement($log);
        $log->setUser(null);
    }

    /**
     * Get logs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLogs()
    {
        return $this->logs;
    }

    /**
     * Add todoAuteur
     *
     * @param Todo $todoAuteur
     *
     * @return User
     */
    public function addTodoAuteur(Todo $todoAuteur)
    {
        $this->todoAuteurs[] = $todoAuteur;

        return $this;
    }

    /**
     * Remove todoAuteur
     *
     * @param Todo $todoAuteur
     */
    public function removeTodoAuteur(Todo $todoAuteur)
    {
        $this->todoAuteurs->removeElement($todoAuteur);
        $todoAuteur->setAuteur(null);
    }

    /**
     * Get todoAuteurs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTodoAuteurs()
    {
        return $this->todoAuteurs;
    }

    /**
     * Add todoDestinataire
     *
     * @param Todo $todoDestinataire
     *
     * @return User
     */
    public function addTodoDestinataire(Todo $todoDestinataire)
    {
        $this->todoDestinataires[] = $todoDestinataire;

        return $this;
    }

    /**
     * Remove todoDestinataire
     *
     * @param Todo $todoDestinataire
     */
    public function removeTodoDestinataire(Todo $todoDestinataire)
    {
        $this->todoDestinataires->removeElement($todoDestinataire);
    }

    /**
     * Get todoDestinataires
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTodoDestinataires()
    {
        return $this->todoDestinataires;
    }

    /**
     * Add essai
     *
     * @param Essais $essai
     *
     * @return User
     */
    public function addEssai(Essais $essai)
    {
        $this->essais[] = $essai;
        return $this;
    }

    /**
     * Remove essai
     *
     * @param Essais $essai
     */
    public function removeEssai(Essais $essai)
    {
        $this->essais->removeElement($essai);
    }

    /**
     * Get essais
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEssais()
    {
        return $this->essais;
    }

    /**
     * Get rulesProtocole
     *
     * @return string
     */
    public function getRulesProtocole()
    {
        return $this->rulesProtocole;
    }

    /**
     * Set rulesProtocole
     *
     * @param string $rulesProtocole
     *
     * @return User
     */
    public function setRulesProtocole($rulesProtocole)
    {
        $this->rulesProtocole = $rulesProtocole;

        return $this;
    }
}

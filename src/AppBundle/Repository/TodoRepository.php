<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Todo;
use AppBundle\Entity\User;

/**
 * TodoRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TodoRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param User $user
     * @return array
     */
    public function findAlertes(User $user)
    {
        $queryBuilder = $this->createQueryBuilder('t')
            ->leftJoin('t.destinataires', 'd')
            ->where("t.dateAlerte <= :now and (t.niveauResolution != '" . TODO::RESOLU . "' and t.niveauResolution != '" . TODO::RESOLU_AVEC_REMARQUES . "') and d = :user and t.alerte = 1")
            ->setParameter("now", new \DateTime())
            ->setParameter("user", $user);

        return $queryBuilder->getQuery()->getArrayResult();
    }

    /**
     * @param User $user
     * @param $lastVisite
     * @return array
     */
    public function findNewTodos(User $user, $lastVisite)
    {
        $queryBuilder = $this->createQueryBuilder('t')
            ->leftJoin('t.destinataires', 'd')
            ->where("d = :user and (t.niveauResolution != '" . TODO::RESOLU . "' and t.niveauResolution != '" . TODO::RESOLU_AVEC_REMARQUES . "')")
            ->setParameter("user", $user);

        if (false && $lastVisite && $lastVisite instanceof \Datetime) {

            $queryBuilder
                ->andWhere("t.createdAt > :lastVisite")
                ->setParameter("lastVisite", $lastVisite);
        }

        return $queryBuilder->getQuery()->getArrayResult();
    }
}
